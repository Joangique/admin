import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ChartsModule} from 'ng2-charts';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {FullComponent} from './layouts/full.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {NavigationComponent} from './shared/header-navigation/navigation.component';
import {SpinnerComponent} from './shared/spinner.component';
import {BreadcrumbComponent} from './shared/breadcrumb/breadcrumb.component';
import {SidebarComponent} from './shared/sidebar/sidebar.component';
import {UsuariosComponent} from './pages/usuarios/usuarios.component';
import { LoginComponent } from './pages/login/login.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

const myPages = [
  DashboardComponent,
  UsuariosComponent
];

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    NavigationComponent,
    BreadcrumbComponent,
    SidebarComponent,
    ...myPages,
    LoginComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    PerfectScrollbarModule,
    ChartsModule,
    AppRoutingModule
  ],
  providers: [{
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
