import {Component, AfterViewInit, OnInit, Output, EventEmitter} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';
import {menu} from '../../menu';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems;
  // tslint:disable-next-line:no-output-rename
  @Output('closeSM') closeSM = new EventEmitter();

  // this is for the open close
  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  // End open close
  ngOnInit() {
    this.sidebarnavItems = menu;
    $(document).ready(() => {
      $('.sidebar-item>a').click(function() {
        const el = this;
        $('.sidebar-item').find('.collapse').removeClass('in').promise().then((x) => {
          $(el).parent().find('.collapse').toggleClass('in');
        });

      });
    });
  }


  closeSideMenu() {

    if ($(window).width() <= 767) {
      this.closeSM.emit();
    }

  }
}
