export const menu = [{
  titulo: 'Principal',
  icono: 'mdi mdi-gauge',
  submenu: [
    {titulo: 'Dashboard', url: 'dashboard'},
  ]
},
  {
    titulo: 'Mantenimientos',
    icono: 'mdi mdi-folder-lock-open',
    submenu: [
      {titulo: 'Usuarios', url: 'usuarios'},
    ]
  }];
