import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FullComponent} from './layouts/full.component';
import {UsuariosComponent} from './pages/usuarios/usuarios.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {LoginComponent} from './pages/login/login.component';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: FullComponent,
    children: [
      {path: '', redirectTo: 'admin/dashboard', pathMatch: 'full'},
      {path: 'usuarios', component: UsuariosComponent},
      {
        path: 'dashboard',
        component: DashboardComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
