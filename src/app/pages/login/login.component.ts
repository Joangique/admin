import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  auth = {
    email: '',
    password: ''
  };

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    if (this.auth.email === 'admin' && this.auth.password === 'admin') {
      this.router.navigateByUrl('admin');
    }

  }
}
